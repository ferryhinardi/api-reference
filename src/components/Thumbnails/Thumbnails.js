import React, { PropTypes } from 'react';
import withStyles from '../../decorators/withStyles';
import styles from './Thumbnails.css';

@withStyles(styles)

export default class Thumbnails {

  static propTypes = {
    maxLines: PropTypes.number
  };

  render() {
    return (
      <div className="col-xs-6 col-md-3">
      	<a href="#" className="thumbnail">
      	  <img src style={{ height:"180px", width:"100px" }} />
      	</a>
      </div>
    );
  }
}